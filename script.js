function getData() {
  fetch(`https://newsapi.org/v2/top-headlines?country=id&apiKey=e39d3819647a4a089371967098049a3e`)
  .then((respon) => respon.json())
  .then((respon) => {
    console.log(respon);
    const data = respon.articles;
    let cards = '';
    data.forEach(a => cards += showCards(a));
    const article= document.getElementById("data");
    article.innerHTML = cards;
  })
}

getData();

// Live search pake enter
var input = document.getElementById("myInput");
input.addEventListener("keypress", function(event) {
  if (event.key === "Enter") {
    const inputKeyword =document.querySelector('.input-keyword');

fetch(`https://newsapi.org/v2/everything?q=${inputKeyword.value}&apiKey=e39d3819647a4a089371967098049a3e`)
  .then(res => res.json())
  .then(res => {
    const data = res.articles;
    let cards = '';
    data.forEach(a => cards += showCards(a));
    document.getElementById("data").innerHTML = cards;
  });
      event.preventDefault();
      document.getElementById("myBtn").click();
    }
  });    

// Live search pake button dan enter juga bisa

// var input = document.getElementById("myInput");
// input.addEventListener("keypress", function(event) {
//   if (event.key === "Enter") {
//     event.preventDefault();
//     document.getElementById("myBtn").click();
//   }
// });

// const searchButton = document.querySelector('.search-button');
// searchButton.addEventListener('click', function () {
//   const inputKeyword =document.querySelector('.input-keyword');

//   fetch(`https://newsapi.org/v2/everything?q=${inputKeyword.value}&apiKey=e39d3819647a4a089371967098049a3e`)
//     .then(res => res.json())
//     .then(res => {
//       const data = res.articles;
//       let cards = '';
//       data.forEach(a => cards += showCards(a));
//       document.getElementById("data").innerHTML = cards;
//     })
// });

function showCards(articles) {
  return `
  <div class="col">
      <div class="card h-100">
      <img 
      width="20"; height="40%"
      src="${articles.urlToImage}" 
      class="card-img-top" alt="..." />
      <div class="card-body">
          <h5 class="card-title" style="font-size:17px">${articles.title}</h5>
          <p class="card-text">
          <small class="text-muted">${articles.author}, ${articles.source.name}</small>
          <p class="card-description">${articles.description}</p>
          </p>
          <a class="btn btn-primary" href="${articles.url}" role="button">Read More</a>
      </div>
      <div class="card-footer">
          <small class="text-muted">${articles.publishedAt}</small>
      </div>
      </div>
  </div>
  `;
}
